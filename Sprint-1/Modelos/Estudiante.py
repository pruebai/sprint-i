from Modelos.AbstractModelo import AbstractModel
"""
    Esta clase se heredara el constructor especializado (con esto se esta ahorrando de copiar y pegar
    el constructor en todas las clases) de la clase abstractModelo , en este caso se esta aplicando 
    un polimorfismo de sobre escritura. 
"""


class Estudiante(AbstractModel):
    pass